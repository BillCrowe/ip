

![IPControlDebugPreview](https://bitbucket.org/BillCrowe/ip/raw/cb40da17174ec960eb6546c524ec6a918502f16d/Screenshot.JPG)

Help File: IP Control Debug v2.0

General Notes:
     The IP Control Debug Text Controller component is intended for use as a testing and troubleshooting tool for controlling third party devices via a network connection. The Debug Window will provide a console to view commands and responses as well as IP connection status changes.

Controls
     Debug Clear (Trigger)- Clears the entries in the Debug Window
     Debug Hex (Boolean)- Enables the Debug Window to display new commands and responses as hexadecimal notation
     IP Address (String)- IP address of the controlled device
     Port Number (Integer)- IP port used by the controlled device
     Command[x] (String)- Command to be sent when the Send button is pressed
     Send[x] (Trigger)- Sends the corresponding Command string
     Notes[x] (String)- Text field to allow for quick notes regarding each command string 
    
Application Notes
     *Hexadecimal notation follows the same rules as the Command Button component
          - Hexadecimal values should be preceded with \x which is case sensitive
          - Hexadecimal notation digits 00 - FF are not case sensitive (\xFF or \xff is acceptable)
          - Delimiter notations of \r (\x0d) and \n (\x0a) are case sensitive


Developer: Bill Crowe
Company: QSC

Released Date: 2020.08.27

Tested Firmware Version: 8.3.3
Tested Hardware: Core 110f

Release Notes:
Version v2.0
     Released Date: 2021.06.28
     Tested Q-Sys Firmware Version: 9.1.1
     Tested Hardware: Core 110f
     Notes: 
         - A Command Buttons component is no longer required for testing command strings
         - Dedicated Command String fields, Send buttons, and Notes fields have been added

Version v1.0 - Initial release
     Released Date: 2020.08.27
     Tested Q-Sys Firmware Version: 8.3.3
     Tested Hardware: Core 110f